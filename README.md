# ESP32芯片通过SPI控制WS2812灯带

ESP32芯片一般可以通过RMT的方式控制LED灯带，但是连接蓝牙或wifi的情况下，灯带会闪烁或不稳定，这里给出使用SPI控制灯带的方案，LED动态效果更加稳定

### 使用方法

第1种 将项目所有文件下载到本地，直接用ESP-IDF环境运行本项目。

第2种 复制components目录下的spi_ws2812组件到你项目中，并通过以下代码调用：

```
//初始化
spi_ws2812_init();

//循环
while(1){
    uint32_t tmp[]={0xFF0000,0x00FF00,0x0000FF}; //Red , Green , Blue
    setLedColorByArr(3,tmp);
    usleep(1000*1000);

    uint32_t tmp1[]={0xFFFF00,0x00FFFF,0xFF00FF}; //Yellow , Tiffany , Pink
    setLedColorByArr(3,tmp1);
    usleep(1000*1000);
}
```
颜色通过如 0x00FFFF 的整数来表示，对应到常用的RGB颜色
例：

红色 0xFF0000

绿色 0x00FF00

蓝色 0x0000FF


### 原理
WS2812的显示信号为每24位控制一个灯的显示。通过SPI数据流的每一个字节，模拟WS2812信号中的1个位，每24个字节，模拟一个灯的控制信号

### 其他参考

通过RMT的方式控制灯带
> https://github.com/JSchaenzle/ESP32-NeoPixel-WS2812-RMT

> https://github.com/FozzTexx/ws2812-demo





