

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "esp_event.h"

#include "spi_ws2812.h"

void app_main(void)
{
    spi_ws2812_init();

    while(1){
        uint32_t tmp[]={0xFF0000,0x00FF00,0x0000FF}; //Red , Green , Blue
        setLedColorByArr(3,tmp);
        usleep(1000*1000);

        uint32_t tmp1[]={0xFFFF00,0x00FFFF,0xFF00FF}; //Yellow , Tiffany , Pink
        setLedColorByArr(3,tmp1);
        usleep(1000*1000);
    }
}
